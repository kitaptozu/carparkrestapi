package com.test.restservice.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DashboardDto {


    private String name;

    private String location;

    private boolean isAvailable;
}
