CREATE TABLE park (
  id INT AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(16) NOT NULL,
  location VARCHAR(100) NOT NULL,
  is_available boolean NOT NULL DEFAULT TRUE,
  modify_date timestamp
);
 
INSERT INTO park (name, location, is_available, modify_date) VALUES
  ('A1', '1 FLOOR', true, FORMATDATETIME(CURRENT_TIMESTAMP(), 'yyyy-MM-dd HH:mm:ss')),
  ('A2', '1 FLOOR', true, FORMATDATETIME(CURRENT_TIMESTAMP(), 'yyyy-MM-dd HH:mm:ss')),
  ('A3', '1 FLOOR', true, FORMATDATETIME(CURRENT_TIMESTAMP(), 'yyyy-MM-dd HH:mm:ss')),
  ('B1', '2 FLOOR', true, FORMATDATETIME(CURRENT_TIMESTAMP(), 'yyyy-MM-dd HH:mm:ss')),
  ('B2', '2 FLOOR', true, FORMATDATETIME(CURRENT_TIMESTAMP(), 'yyyy-MM-dd HH:mm:ss')),
  ('B3', '2 FLOOR', true, FORMATDATETIME(CURRENT_TIMESTAMP(), 'yyyy-MM-dd HH:mm:ss'));
