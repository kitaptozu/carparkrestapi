package com.test.restservice;

import com.test.restservice.repository.ParkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication(scanBasePackages = {"com.test"})
@EnableJpaRepositories
@EnableSwagger2
public class RestServiceApplication {


    public static void main(String[] args) {
        SpringApplication.run(RestServiceApplication.class, args);
    }

}
