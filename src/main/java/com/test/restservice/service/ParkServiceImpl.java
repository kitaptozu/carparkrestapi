package com.test.restservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.restservice.model.Park;
import com.test.restservice.dto.DashboardDto;
import com.test.restservice.dto.SensorParamDto;
import com.test.restservice.repository.ParkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ParkServiceImpl implements ParkService {

    @Autowired
    private ParkRepository parkRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public List<DashboardDto> getDashboard() {

        List<Park> parkAreas = parkRepository.findAll();

        List<DashboardDto> dashboardDtoList = parkAreas.stream().map(p -> objectMapper.convertValue(p, DashboardDto.class)).collect(Collectors.toList());

        return dashboardDtoList;
    }

    @Override
    public boolean updateParkStatus(SensorParamDto sensorParamDto) {
        boolean isUpdated;
        int resultOfUpdate = parkRepository.updateStatusOfParkArea(sensorParamDto.getName(), sensorParamDto.isAvailable());

        if (resultOfUpdate > 0) {
            isUpdated = true;
        } else {
            isUpdated = false;
        }
        return isUpdated;
    }

    @Override
    public boolean isParkAreaAvailable() {
        boolean isAvailable;

        Integer countOfAvailableAreas = parkRepository.countOfAvailableParkAreas();

        if (countOfAvailableAreas > 0) {
            isAvailable = true;
        } else {
            isAvailable = false;
        }

        return isAvailable;
    }
}
