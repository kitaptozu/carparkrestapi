package com.test.restservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.restservice.dto.SensorParamDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class ParkingControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;


    @Test
    void test1Sensor() throws Exception {

        SensorParamDto busy = new SensorParamDto("A1", Boolean.FALSE);

        this.mockMvc.perform(
                post("/parking/updateStatus/")
                        .content(objectMapper.writeValueAsString(busy))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("true"));

        SensorParamDto free = new SensorParamDto("A1", Boolean.TRUE);

        this.mockMvc.perform(
                post("/parking/updateStatus/")
                        .content(objectMapper.writeValueAsString(free))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    void test2InGate() throws Exception {

        this.mockMvc.perform(
                get("/parking/checkParking/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
        )
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string("true"));
    }

    @Test
    void test3Dashboard() throws Exception {

        this.mockMvc.perform(
            get("/dashboard/getDashboard/")
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
            )
            .andDo(print()).andExpect(status().isOk())
            .andExpect(jsonPath("$[0].name").value("A1"))
            .andExpect(jsonPath("$[0].location").value("1 FLOOR"))
            .andExpect(jsonPath("$[0].available").value(true))
            .andExpect(jsonPath("$[1].name").value("A2"))
            .andExpect(jsonPath("$[1].location").value("1 FLOOR"))
            .andExpect(jsonPath("$[1].available").value(true))
            .andExpect(jsonPath("$[2].name").value("A3"))
            .andExpect(jsonPath("$[2].location").value("1 FLOOR"))
            .andExpect(jsonPath("$[2].available").value(true))
            .andExpect(jsonPath("$[3].name").value("B1"))
            .andExpect(jsonPath("$[3].location").value("2 FLOOR"))
            .andExpect(jsonPath("$[3].available").value(true))
            .andExpect(jsonPath("$[4].name").value("B2"))
            .andExpect(jsonPath("$[4].location").value("2 FLOOR"))
            .andExpect(jsonPath("$[4].available").value(true))
            .andExpect(jsonPath("$[5].name").value("B3"))
            .andExpect(jsonPath("$[5].location").value("2 FLOOR"))
            .andExpect(jsonPath("$[5].available").value(true));


    }
}
