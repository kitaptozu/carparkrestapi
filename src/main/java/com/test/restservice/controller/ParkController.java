package com.test.restservice.controller;

import com.test.restservice.dto.DashboardDto;
import com.test.restservice.dto.SensorParamDto;
import com.test.restservice.service.ParkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ParkController {

    @Autowired
    private ParkService parkService;

    @GetMapping("/dashboard/getDashboard/")
    public ResponseEntity<List<DashboardDto>> getDashboard() {
        List<DashboardDto> parkAreas = parkService.getDashboard();
        return new ResponseEntity<>(parkAreas, HttpStatus.OK);
    }

    @GetMapping("/parking/checkParking/")
    public ResponseEntity<Boolean> checkParking() {

        boolean isAvailable = parkService.isParkAreaAvailable();

        return new ResponseEntity<>(isAvailable, HttpStatus.OK);
    }

    @PostMapping("/parking/updateStatus/")
    public ResponseEntity<Boolean> updateStatus(@RequestBody SensorParamDto sensorParamDto) {

        boolean isSuccess = parkService.updateParkStatus(sensorParamDto);

        return new ResponseEntity<>(isSuccess, HttpStatus.OK);
    }


}
