package com.test.restservice.repository;

import com.test.restservice.model.Park;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

public interface ParkRepository extends JpaRepository<Park, Integer> {

    @Transactional
    @Modifying()
    @Query("UPDATE Park p SET p.isAvailable = :isAvailable, p.modifyDate = current_timestamp WHERE p.name = :name")
    int updateStatusOfParkArea(@Param("name") String name, @Param("isAvailable") boolean isAvailable);

    @Query("SELECT COUNT(p) FROM Park p WHERE p.isAvailable = true")
    Integer countOfAvailableParkAreas();
}
