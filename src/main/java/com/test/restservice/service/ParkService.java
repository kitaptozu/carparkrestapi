package com.test.restservice.service;

import com.test.restservice.dto.DashboardDto;
import com.test.restservice.dto.SensorParamDto;

import java.util.List;

public interface ParkService {

    List<DashboardDto> getDashboard();

    boolean updateParkStatus(SensorParamDto sensorParamDto);

    boolean isParkAreaAvailable();
}
